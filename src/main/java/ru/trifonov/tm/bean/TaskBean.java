package ru.trifonov.tm.bean;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.annotation.SessionScope;
import ru.trifonov.tm.api.ITaskService;
import ru.trifonov.tm.model.Project;
import ru.trifonov.tm.model.Task;

import javax.inject.Named;
import java.util.List;

@Getter
@Setter
@Named("taskBean")
@SessionScope
public class TaskBean {
    @NotNull
    ITaskService taskService;
    @Nullable
    private Project project;
    @Nullable
    private Task task;
    @Nullable
    private String projectId;
    @Nullable
    private String id;
    @Nullable
    private String name;
    @Nullable
    private String description;
    @Nullable
    private String dateBegin;
    @Nullable
    private String dateEnd;
//    @Nullable
//    private List<Task> tasks;
    @Nullable
    private List<Task> tasksByProjectId;

    @Autowired
    public TaskBean(@NotNull ITaskService taskService) {
        this.taskService = taskService;
    }

    @NotNull
    public List<Task> getTasks() {
        return taskService.findAll();
    }

    @NotNull
    public List<Task> getTasksByProjectId() {
        return taskService.findAllByProjectId(projectId);
    }

    @NotNull
    public String setProjectId(@NotNull final String projectId) {
        System.out.println("setProjectId");
        this.projectId = projectId;
        return "/views/tasksByProject.xhtml?faces-redirect=true";
    }

    @NotNull
    public String find(@Nullable final String id) {
        task = taskService.find(id);
        return "/views/taskView.xhtml?faces-redirect=true";
    }

    @NotNull
    public String delete(@Nullable final String id) {
        taskService.delete(id);
        return "/views/tasks.xhtml?faces-redirect=true";
    }

    @NotNull
    public String edit(
            @Nullable final String id,
            @Nullable final String projectId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final String dateBegin,
            @Nullable final String dateEnd
    ) {
        System.out.println(id);
        System.out.println(projectId);
        System.out.println(name);
        System.out.println(description);
        System.out.println(dateBegin);
        System.out.println(dateEnd);
        taskService.update(id, projectId, name, description, dateBegin, dateEnd);
        return "/views/tasksByProject.xhtml?faces-redirect=true";
    }

    @NotNull
    public String create(
            @Nullable final String projectId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final String dateBegin,
            @Nullable final String dateEnd
    ) {
        taskService.insert(projectId, name, description, dateBegin, dateEnd);
        return "/views/tasksByProject.xhtml?faces-redirect=true";
    }

    @NotNull
    public String setId(@Nullable final String id, @Nullable final String projectId) {
        this.id = id;
        this.projectId = projectId;
        return "/views/taskEdit.xhtml?faces-redirect=true";
    }
}
