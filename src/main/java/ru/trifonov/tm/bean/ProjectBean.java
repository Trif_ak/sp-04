package ru.trifonov.tm.bean;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.annotation.SessionScope;
import ru.trifonov.tm.api.IProjectService;
import ru.trifonov.tm.model.Project;

import javax.inject.Named;
import java.util.List;

@Getter
@Setter
@Named("projectBean")
@SessionScope
public class ProjectBean {
    @NotNull
    IProjectService projectService;
    @Nullable
    private Project project;
    @Nullable
    private String id;
    @Nullable
    private String name;
    @Nullable
    private String description;
    @Nullable
    private String dateBegin;
    @Nullable
    private String dateEnd;

    @Autowired
    public ProjectBean(@NotNull IProjectService projectService) {
        this.projectService = projectService;
    }

    @NotNull
    public List<Project> getProjects() {
        System.out.println("find all projects");
        return projectService.findAll();
    }

    @NotNull
    public String find(@Nullable final String id) {
        System.out.println("bean - find project");
        System.out.println("Id of project " + id);
        project = projectService.find(id);
        System.out.println(project);
        return "/views/projectView.xhtml?faces-redirect=true";
    }

    @NotNull
    public String delete(@Nullable final String id) {
        System.out.println("delete project");
        projectService.delete(id);
        return "/views/projects.xhtml?faces-redirect=true";
    }

    @NotNull
    public String edit(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final String dateBegin,
            @Nullable final String dateEnd
    ) {
        projectService.update(id, name, description, dateBegin, dateEnd);
        this.id = null;
        this.name = null;
        this.description = null;
        this.dateBegin = null;
        this.dateEnd = null;
        return "/views/projects.xhtml?faces-redirect=true";
    }

    @NotNull
    public String create(
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final String dateBegin,
            @Nullable final String dateEnd
    ) {
        projectService.insert(name, description, dateBegin, dateEnd);
        this.name = null;
        this.description = null;
        this.dateBegin = null;
        this.dateEnd = null;
        return "/views/projects.xhtml?faces-redirect=true";
    }

    @NotNull
    public String setId(@Nullable final String id) {
        System.out.println("set id project");
        System.out.println(id);
        this.id = id;
        return "/views/projectEdit.xhtml?faces-redirect=true";
    }
}
