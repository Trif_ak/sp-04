package ru.trifonov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.trifonov.tm.api.IProjectService;
import ru.trifonov.tm.model.Project;
import ru.trifonov.tm.repository.IProjectRepository;

import java.util.Date;
import java.util.List;

@Service
public final class ProjectService extends AbstractService implements IProjectService {
    @NotNull
    @Autowired
    IProjectRepository projectRepository;

    @Override
    @SneakyThrows
    public void insert(
            @Nullable final String name, @Nullable final String description,
            @Nullable final String beginDate, @Nullable final String endDate
    ) {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct name.");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct description.");
        if (beginDate == null) throw new NullPointerException("Enter correct begin date.");
        if (endDate == null) throw new NullPointerException("Enter correct end date.");
        @NotNull final Project project = new Project(
                name,
                description,
                dateFormat.parse(beginDate),
                dateFormat.parse(endDate)
        );
        projectRepository.save(project);
    }

    @SneakyThrows
    @Override
    public void update(
            @Nullable final String id, @Nullable final String name, @Nullable final String description,
            @Nullable final String beginDate, @Nullable final String endDate
    ) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct name.");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct description.");
        if (beginDate == null) throw new NullPointerException("Enter correct begin date.");
        if (endDate == null) throw new NullPointerException("Enter correct end date.");
        @NotNull final Project project = new Project(
                id,
                name,
                description,
                dateFormat.parse(beginDate),
                dateFormat.parse(endDate)
        );
        projectRepository.save(project);
    }

    @Override
    public List<Project> findAll() {
        @Nullable List<Project> projects = projectRepository.findAll();
        if (projects == null  || projects.isEmpty()) throw new NullPointerException("Projects not found.");
        return projects;
    }

    @Override
    public Project find(@Nullable final String id) {
        System.out.println("service");
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        if (!projectRepository.findById(id).isPresent()) throw new NullPointerException("Project not found.");
        @NotNull Project project = projectRepository.findById(id).get();
        return project;
    }

    @Override
    public void delete(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        projectRepository.deleteById(id);
    }
}